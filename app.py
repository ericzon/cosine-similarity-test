# Problema: solo encuentra coincidencia si el matching es exacto

documents = (
"The sky is blue",
"The sun is bright",
"The sun in the sky is bright",
"otra frase mas blue is the sky",
"otra frase mas the sky is blue",
"otra frase mas the sky sky sky sky sky sky sky sky sky is blue",
"that skies are blues",
# "the sky is blue"
)

print "DOCS size: "
print len(documents)
print documents

from sklearn.feature_extraction.text import TfidfVectorizer

tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
print "TF-IDF Shape:"
print tfidf_matrix.shape # (4,11)

from sklearn.metrics.pairwise import cosine_similarity

print "Cosine similarity between the first document and the others: "
cs = cosine_similarity(tfidf_matrix[0:1], tfidf_matrix)
print cs # array([[ 1. ,  0.36651513,  0.52305744,  0.13448867]])

maxPos = 0
maxValue = -1
for csIdx, csValue in enumerate(cs[0]):
  print "comparo csValue %s" % csValue + " con maxPos %s " % maxPos
  if csIdx > 0 and csValue > maxValue:
    maxPos = csIdx
    maxValue = csValue

print "maxPos: "
print maxPos

print "maxValue: "
print maxValue

print "Similar document: %s " % documents[maxPos]

import math

cos_sim = maxValue
if str(cos_sim) == "1.0":
  print "Son iguales!!"
else:
  angle_in_radians = math.acos(cos_sim)
  print "Math degrees: "
  print math.degrees(angle_in_radians)

print "Bye bye!"

# print sorted(cosine_similarity(tfidf_matrix[0:1], tfidf_matrix)[0], reverse=True)

